# An Introduction to the Theory of the "Five Biological Laws of Nature" to Base Understanding on for Scientific Analysis

## Abstract

In evolution, species survive by adapting to their habitat. This involves developing organs to instinctively solve common problems and discovering strategies to survive situations in which their biological needs are conflicted. Those emergencies seem to lurk everywhere after birth and challenge keeping the organism in balance but as the creature matures, become less frequent with more regular life.

The theory of the "Five Biological Laws of Nature" observed by Dr. Ryke Geerd Hamer proposes in five theses:

* How exactly this process takes place.
* How microbes play a role in a symbiotic relationship.
* How in nature it makes sense but in modern society it sometimes does not anymore because the relatively tiny time frame in which it developed did not let species living in it adjust biologically to a completely different lifestyle.

Some symptoms of this, so far assumed to be mistakes of nature and labeled as diseases, can in this context be reevaluated and treated more efficiently and in some cases, effectively for the first time.

The chance to properly verify and potentially make use of this possible breakthrough is as of writing mostly hindered by the lack of scientific discourse about it. This text aims at helping solve that problem by offering a low entry barrier through a short but objective introduction to the topic in order to support either finally debunking or establishing it as a valid step in medical research.

## Conventions

This text is structured for a first time read from the beginning to the end. Uncertainties at any point may be answered in a later section.

Different publications were inconsistent about including his doctor title when addressing Dr. Hamer after his medical license has been revoked. Since this happened after he initially outlined this theory, the following text will generally refer to him as before the revocation and include the title.

<!-- \clearpage -->

## Credit

Full credit for originally observing and outlining those theses goes to the late Dr. Ryke Geerd Hamer, even though he later expressed contradicting ideas meant to be additions.

### Legal Notice About Trade Marks

_Entering `(inh="Hamer, Bona, Sandefjord, NO")` into `Entry field` in this form: <https://register.dpma.de/DPMAregister/marke/experte?lang=en> and clicking the `Start search` button at the bottom will produce a list of his German trade marks, some of which might be referred to in this text._

## Scope

This text attempts to explain the "Five Biological Laws of Nature" shortly in an objective way to help understanding them quickly and raising the scientific discourse to a more constructive level. It does mention the superset of the "Germanic New Medicine" but does not cover it beyond correcting misunderstandings which result from applying that mindset.

|Text Section|Content|
|-|-|
|Part A: The Theory|The theory of the laws themselves as proposed facts. **This whole part is to be read as the central "What if".**|
|Part B: Prospects|Some possible medical breakthroughs provided by the confirmation of the theory.|
|Part C: Resources|Resources to learn more details from and possible supporting evidence.|
|Part D: Common Arguments|Discussing some of the common miunderstandings and arguments.|
|Part E: Communication|How things apparently got out of hand and suggestions on how to better handle the topic.|

<!-- \clearpage -->

## Part A: The Theory

### Thesis "Law Five": In Nature, Everything Makes Sense

#### Evolution

To have survival, there first needs to be lifeforms. Those stay alive individually and eventually produce offspring to turn into a whole species. This allows for evolution: The introduction of minimal random changes called mutations over many generations to have more species.

#### Biological Needs

Being alive isn't a given status in a world based on the laws of physics, an organism needs to engage in some kind of metabolism to sustain itself. For example, to basic lifeforms, populating a habitat means:

* Food needs to be gathered from the environment and eaten.
* Own genetic material needs to be turned into offspring.

More complex lifeforms have more complex needs. Only if the needs are fulfilled can the organism live and the species thrive.

#### Natural Selection

With biological needs come chances to die by failing to fulfill them. Natural selection is the balancing other half of evolution, testing how exactly those chances are influenced by the mutations. Species have to deal with them for aeons of generations until eventually, for each inferior one, there simply is no creature left that managed to survive with it because it turned out to be a handicap compared to other creatures' mutations that allowed for better survival.

#### Natural Order

With cycles of evolution and natural selection, an inevitable process to produce the optimal lifeform establishes the natural environment: The ever changing big picture makes sense as a whole, whatever doesn't work in symbiosis with it fades away.

<!-- \clearpage -->

#### Biological Conflicts as Problems

Creatures managing to stay alive do so by recognizing unfulfilled biological needs as conflicting with their ongoing survival. Not being able to immediately solve a conflict turns it into a problem that needs to be worked on.

Example: Gathering food means to find a way to move the body to it, prepare and eat it.

#### Organs as Solved Problems

<!-- This paragraphs first line still sounds a bit too thesaurusphilic. Revisit some day with fresh eyes to rephrase simpler. -->

The problems of fulfilling biological needs are commonly tasks that can be accomplished following similar principles. Once a mutation has established itself as beneficial in helping with one of those tasks, mutations follow improving upon it so an organ specialized in carrying out the task evolves.

Example: Richard Dawkins demonstrates the evolution of the eye:

<https://www.youtube.com/watch?v=2X1iwLqM2t0>

#### Instinct as an Inherent Skillset

Recognizing biological conflicts and working on problems in order to solve said conflicts requires the skills to use those body parts which evolved to serve as the necessary tools. Since basics like moving muscles and the general principles behind learning stay the same for a species, they evolve into inherited behaviour already known to the newly born offspring in the form of instincts. Newborns don't need to consciously recognize biological conflicts, they just instinctively feel them as urges.

#### Collecting Experience as Character Development

For a newborn, there is no such thing as regular life. Everything is new and every feeling of uncomfortableness is a life threatening conflict. The first moments in life, having a parent caring for it, guiding its instinct and helping it make sense of things is practical. Growing up, experience in handling those conflicts is collected and new ones get less frequent. Being experienced enough in dealing with conflicts leads to maturity with similar conflict patterns turning into less dramatic problems. The older the creature manages to get, the more problem solving turns into chores of applying known solutions. Eventually, the sum of known solutions define how an individual creature behaves and form its character.

<!-- \clearpage -->

### Thesis "Law One": Conflict Activation

#### First Time Conflicts

The very moment the instinct subliminally identifies a new biological conflict is called "Dirk Hamer Syndrome" or DHS for short. The surrounding stimuli are memorized to optimize recognizing similar situations in the future.

<!-- Replace the Psychosomatic Misunderstanding with more details here? -->

When the creature consciously realizes there's a problem, the DHS has already passed and the realization itself is part of the process to solve said problem. This makes the difference between biological conflict and problem more obvious: The instinct recognizes the conflict from which a problem emerges which unless solved immediately through reflexive action can then be worked on by the conscious body.

##### Criterion One: Activation

For a DHS to occur, the situation needs to be an emergency. To the instinct, that is the case when the folloring three requirements all apply at the same time:

|The situation is perceived to be|Which means|
|-|-|
|Acutely dramatic|The creatures way of life is at stake this very moment. This seems to range from figuratively being caught on the wrong foot to being aware a single wrong action might lead to immediate death. It needs to be resolved as soon as possible.|
|Unexpected|There was no way of knowing this would happen, no possible way of planning how to get out of this ahead of time.|
|Isolative|The creature is alone in this. There is no feeling safe in a group, no sharing the concerns.|

<!-- \clearpage -->

##### Criterion Two: Conflict Topic

To properly explain this, a digression about truth:

<!-- Would it make a difference to rephrase this in the spirit of "The most recent stimuli give the littlest context"? -->

Reality is what is actually happening in the world and virtually impossible to know in every detail, especially in a situation that has only just developed into what it is. A creature can rationally form an opinion of what approximately happened in the past and believe it as a world view. With life experience, recently received stimuli can be processed into an idea of what just happened and a guess of what is currently happening. The most recent stimuli give the littlest time to make sense of so their interpretation is the most subjective.

The instinct is always on alert and prioritizes survival in the moment over the annoyance of false alerts so its subliminal analysis is completely subjective. **The creatures perception is constantly checked for the biological needs of the organism and once a situation leads to a DHS, the reaction depends on what need is conflicted based on what the situation biologically seems like to the instinct and what the creatures world view is, not what is really happening.**

The subjectivity of the subliminal instincts situational interpretation make conflict activation and resolve the two technically unpredictable, possibly even completely irrational, key moments in the process.

Multiple creatures can face the same situation but react in completely different ways because each of them have their own lives with different world views that lead to this situation having respectively different meaning to them.

<!-- \clearpage -->

##### Criterion Three: Synchronicity

<!-- Emphasize the Unity more? Like "Synchronicity in Unity" or "Unity in Synchronicity" as title? -->

A problem with the urgency of a biological need being conflicted means to **temporarily adjust the respective parts of the body to the present situation. The psyche, the responsible organ and its relay in the brain are in unity**, knowing the state of one leads to the ability of deriving the state of both others, modifying one leads to the respective change in both others. This means there's no need to learn the states of all three layers of the unit, just one is sufficient. Redundantly available information from the other layers can be used in support to confirm the reading.

|Layer|Adjustment|
|-|-|
|Psyche|Emotions and thoughts are aligned to the problem to be solved. Urge for agency tends towards related actions.|
|Organ|The specific tissue evolutionarily responsible for solving the present problem is physically adjusted to fulfill it optimally.|
|Brain|The relay for the responsible organ develops a "Hamerscher Herd" (Hamer Focus), HH for short. This is a target formation visible in a CCT scan. _As of writing, it's not yet known what exactly a HH is organically or how its visual representation comes to be._|

<!-- \clearpage -->

#### Repetitions

The DHS only happens for the first time the conflict is encountered. If it's solved, the memorized stimuli of the DHS situation in connection with the knowledge of what specific conflict was reacted with serves to provide the instinct with the base skill of how to recognize the situation in the future so the same successful strategy can be applied again.

##### Relapses

The simple situation of the conflict topic appearing again is called a "relapse". It's basically the same as the initial program, just without the necessity of a DHS.

##### Tracks

Since the surrounding stimuli were memorized when the DHS occured, the creatures instinct is now actively on the lookout for them by filtering the perception, the memory of one acts like one of multiple "track" now running through the creatures life subliminally. Whenever one of those stimuli is perceived again, the same strategy is repeated.

<!-- \clearpage -->

### Thesis "Law Two": Balancing Body Activity in Phases

_Also known as "The Law of Two Phases"._

An active biological conflict is an urgent temporary measure meant to assist the creature in living past the current situation. After the conflict is solved, the changes aren't returned to the original state but undone to the same level of normality. This happens in a balancing maintenance phase which the creature experiences as needing to relax and going easy on the respective tissue. A whole cycle of shifting towards adjusting to a problem and normalizing again in the aftermath to return to normal life afterwards is called a "Sinnvolles Biologisches Sonderprogramm" in German (Meaningful Biological Special Program) or SBS for short.

In detail, the phases happen like this:

#### Normality

A theoretical state of regular life. Practically, the creature started to learn how to survive in life the moment it was born so there's probably always some other SBS active. The biological need in question however is fulfilled, the psyche has no compelling urge to think about it, the organ works routinely.

|Layer|Adjustment|
|-|-|
|Psyche|Normal feelings, free thinking.|
|Organ|Working routinely.|
|Brain|Normal visuals in CCT scan of the respecive brain area.|

#### Conflict Start

A conflict to the biological need is perceived subjectively by the instinct. Either a DHS occurs or knowledge from an earlier DHS is used to start a SBS.

<!-- Mention the possibility of falling back here from later phases or TMI for a short version? -->

<!-- \clearpage -->

#### Conflict Active

_Short: CA._

Depending on the complexity of the problem, attempting to solve it can happen in a range of possible reactions:

* The instinct realizes a false alert and considers the problem solved.
* Reflexive action performed before the consciousness notices there was a problem.
* Ongoing, conscious full body effort.

The integral of temporal length and intensity of the process result in the "conflict mass" which increases during this phase.

If the problem can't be solved, the creature never ends this phase and eventually dies from exhausting itself still trying to reach a solution. Otherwise the chosen strategy eventually works out to solve the problem, the creature lives and moves on to the next phase of the SBS.

|Layer|Adjustment|
|-|-|
|Psyche|Emotions of stress, more obsessively forced thinking about the problem and how to solve it, feeling the urge for specific actions that help in doing so.|
|Organ|Depending on what type the responsible tissue is of, functionality is either physically increased or decreased to better meet the needs of the situation. The more the conflict mass grows, the more measurable this leads to cellular alterations.|
|Brain|The HH is now visible as a target formation in the respective brain area. Starting practically invisible, the more the conflict mass grows, the more clearly the formation can be identified.|

<!-- \clearpage -->

#### Conflict Solved

_Short: CL._

While eventually the consciousness may predictably identify a state of having solved the problem, this moment is only reached when and if the instinct subjectively perceives it biologically. It's the counterpart to the Conflict Start and initiates the balancing second half of the SBS.

#### Post Conflictolysis A

_Alternatively: Swelling Phase. Short: PCL-A._

The body starts to reverse the temporary measures needed to solve the problem and survive. The temporal length and intensity of PCL-A and later PCL-B combined depend on how long and intense the measures of the CA phase were. The measurements may vary but the total conflict mass that was built requires equal maintenance work to reduce.

|Layer|Adjustment|
|-|-|
|Psyche|Emotions of calmness, being freed of the pressure, ability to think freely, a relaxed state.|
|Organ|Maintenance work to reverse the functionality adjustment. Has there been an increase in cells, they're now being removed. Has there been a decrease in cells, they're now growing back, both possibly impairing functionality or causing pain. Swellings can be caused by water storage as the process follows the principle of wound healing.|
|Brain|Water is stored to dissolve the HH. Instead of the target formation, now bodies of water would be visible in the CCT scan of the respective brain area. Depending on the conflict mass, this may lead to swellings applying pressure to the surrounding regions perceived as some kind of headache, possibly impairing the functionality of surrounding brain areas.|

<!-- \clearpage -->

#### Epileptoid Crisis

_German: "Epileptoide Krise", short: EK._

The climax of the maintenance work, something of a cleanup. A short phase resembling CA that's more intense than the CA phase was, possibly by a multiple. Should one EK not suffice to do the work necessary, the body can need multiple attempts. It initiates the return to normality. Since the increased urination transitions into PCL-B, the EK is sometimes also recognized as the start of a "Urination Phase".

|Layer|Adjustment|
|-|-|
|Psyche|The conflict is relived in a time lapse. _(This seems to only happen consciously in cases of extremely huge conflict masses.)_|
|Organ|The organ reenacts the problem solving. Depending on the tissue, this might be measurable as a seizure. The stored water is released leading to the respective increase in urination.|
|Brain|The stored water and with it practically the HH are released leading to the respective increase in urination.|

#### Post Conflictolysis B

_Alternatively: Scarring Phase. Short: PCL-B._

The second PCL phase ends the SBS and returns the organism to normality, leaving behind scarring and connective tissue in brain and organ.

|Layer|Adjustment|
|-|-|
|Psyche|Relaxation turns into feeling normal.|
|Organ|Scars and connective tissue form, swelling flattens, the tissue normalizes back to a state similar to its original one.|
|Brain|The HH turns into normal brain tissue with scars and connective tissue.|

<!-- \clearpage -->

### Thesis "Law Three": Mapping Tissue Type to Adjustment Strategy

_Also known as "The Ontogenetic System of Meaningful Biological Special Programs"._

#### Divisions into Tissue Types

While individual tissues have their own specific job to do in the body, they can all be categorized by the part of the brain responsible for controlling them, generalizing them into groups of similar behaviour. The main differentiation is between old and new brain:

**Tissues controlled by the old brain increase their functionality during CA phases and decrease it during PCL phases. For tissues controlled by the new brain it's the other way around: During CA phases, their functionality is decreased while during PCL phases, it's increased.**

Those two groups are usually divided again into two each, making them four types of tissues in total for humanoid creatures. This time, the differentiation happens based roughly on which germ layer the cells form from in embryonic development, categorizing them into general biological topics.

<!-- \clearpage -->

<!-- Very awkward page splits here. But I don't see how to format the germ layers into tables that would work better than this. -->

#### Tissue Type by Germ Layer

The following is only a generalized overview, in-depth resources have detailed lists of tissues.

##### Endoderm

<!-- Leaving out "Perception of morsels, reproduction" for now because it's confusing with perception and sexuality in ectoderm. Also this is explicitly a "generalized overview". -->

* Controlled by the brainstem.
    * CA: Increase in functionality and cell growth.
    * PCL: Decrease in functionality and cell reduction.
* Biological topic: Metabolism, morsels.
    * Tissues responsible for metabolism: Gathering, absorption, analysis, processing and discharge of vital substances.
    * Conflict topic related to: Metabolism of vital substances, acquisition, analysis, processing or getting rid of things biologically perceived as morsels.
* EK duration of up to four hours.

##### Old Mesoderm

* Controlled by the cerebellum.
    * CA: Increase in functionality and cell growth.
    * PCL: Decrease in functionality and cell reduction.
* Biological topic: Defense.
    * Tissues responsible for protection of the organism.
    * Conflict topic related to: Integrity of ones own body or that of others, being attacked, getting stained, feeling insulted.
* EK duration of up to 45 minutes.

##### New Mesoderm

* Controlled by the cerebral medulla.
    * CA: Decrease in functionality and cell reduction.
    * PCL: Increase in functionality and cell growth.
* Biological topic: Performance.
    * Tissues responsible for stability of the body, movement and performance.
    * Conflict topics related to: Local conflicts of specific body parts failing at their respective task, stable stance, performing well, being strong, self-esteem.
* EK duration of up to 60 seconds.

<!-- Sorry, ectoderm, see you on the other side. -->

<!-- \clearpage -->

##### Ectoderm

* Controlled by the cerebral cortex.
    * CA: Decrease in functionality and cell reduction.
    * PCL: Increase in functionality and cell growth.
* Biological topic: Perception, territory, interaction, social connections.
    * Tissues responsible for sensory perception, interaction with the environment, social contacts, sexuality.
    * Conflict topics related to: Perception, territory, social status and relationship changes like partnering up and separating, sexuality.
* EK duration of up to 20 seconds.

### Body Side

<!-- Explain more explicitly this is not a "law" thesis but just a rule based on experience? -->

For symptoms not locally linked to the conflicted body part, experience shows two rulesets define which side of the symmetrical body halves is affected:

* For the brainstem, it's the principle of "Way in to the right, way out to the left".
* For the other tissues, it depends on which side of the individuals brain is biologically dominant.

There's multiple ways of varying accuracy to determine which brain side the biologically dominant one is, one commonly used test is to check the handedness through clapping. Right-handed people are left-brainers, left-handed people are right-brainers.

For **right-handed** people:

|Body Side|Topic|Examples|
|-|-|-|
|Right|Strong, about equal peers|Partner, friend, colleague, father, enemy, society, ...|
|Left|Weak, about hierarchically different peers|Mother, child, pet, respected people, nursing cases, ...|

<!-- \clearpage -->

For **left-handed** people, it's the same, just both sides switched:

|Body Side|Topic|Examples|
|-|-|-|
|Left|Strong, about equal peers|Partner, friend, colleague, father, enemy, society, ...|
|Right|Weak, about hierarchically different peers|Mother, child, pet, respected people, nursing cases, ...|

### Thesis "Law Four": Symbiotic Relationship to Microbes

_Also know as "The Ontogenetic System of Microbes"._

The human body isn't doing all the adjustment work itself. The microbes that evolved living inside it symbiotically act as maintenance workers and remove the temporarily needed cells from CA that after CL are excess during PCL. Similar to tissue types, they can be categorized by germ layer:

|Germ layer  |Microbes                         |
|-|-|
|Endoderm    |Fungi, mycobacteria, tuberculosis|
|Old Mesoderm|Fungi, mycobacteria, tuberculosis|
|New Mesoderm|Bacteria                         |
|Ectoderm    |Viruses                          |

