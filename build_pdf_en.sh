#!/usr/bin/env sh


# Clearly mark start of execution, explicitly naming the script file as given in $0.
echo "[info] Starting ${0}:"


###
# CONFIGURATION
###


# Neat round number of MEBIBYTES of available drive space to pass the free space test.
# (Mebibyte = Megabyte explicitly using the binary (1024),
# not the decimal (1000) multiplier.)
#
# REMEMBER TO UPDATE THIS AS THE DOCUMENT GROWS!!
DRIVE_SPACE_NEEDED=1

# URL of homepage for footer link. Escaped for placement in sed.
URL_HOMEPAGE='https:\/\/gitlab.com\/litespark\/'

# Input file names.
FILE_NAME_PAPER='5bl_paper_en.md'
FILE_NAME_HEADERINCLUDES='headerincludes_pdf.yaml'

# Translation of the original English heading "Credit" so it can be found in the text.
# Used to place the table of contents before it.
HEADING_CREDIT='Credit'

# Start of the output file name.
OUTPUT_NAME='5bl_paper_en'

# Name of the verbose log file given by debug mode.
DEBUG_LOG_FILE_NAME='5bl_paper_en_pandoc_verbose.log'


###
# DEPENDENCIES
###


# This script should run in any POSIX compliant shell with all dependencies available.
# To learn what those dependencies are, run with --listdeps as mentioned in
# below line which will be echoed should dependencies be detected as missing.
LISTDEPS_INFO='[info] Run with --listdeps to list all dependencies.'

# All programs necessary to run successfully.
#
# Assuming shell core functionalities to work:
# (Single / double quotes), (variables), case, echo, exit, for, if,
# command, test / [, (functions), return, trap, (pipes) and (redirections).
#
# Not testing for rm as it's not needed in debug mode.
# Not testing for cat, date, git, sha256sum as they're technically optional.
PROGRAM_DEPENDENCIES='kpsewhich df tail awk mktemp grep cut sed tr sort pandoc pdflatex'

# All LaTeX files required to build the PDF,
# as extracted from the verbose log file.
LATEX_DEPENDENCIES='amsbsy.sty amsfonts.sty amsgen.sty amsmath.sty amsopn.sty amssymb.sty amstext.sty array.sty article.sty atbegshi-ltx.sty atveryend-ltx.sty auxhook.sty bigintcalc.sty bitset.sty bkm-pdftex.def bookmark.sty booktabs.sty calc.sty color.cfg colortbl.sty epigraph.sty epstopdf-base.sty epstopdf-sys.cfg etexcmds.sty etoolbox.sty fancyhdr.sty fontenc.sty footnotehyper.sty gettitlestring.sty graphics.sty graphics.cfg graphicx.sty hpdftex.def hycolor.sty hyperref.sty hyperref-langpatches.def iftex.sty ifthen.sty infwarerr.sty inputenc.sty intcalc.sty keyval.sty kvdefinekeys.sty kvoptions.sty kvsetkeys.sty l3backend-pdftex.def letltxmacro.sty lmodern.sty longtable.sty ltxcmds.sty microtype.sty microtype-pdftex.def microtype.cfg mt-cmr.cfg mt-msa.cfg mt-msb.cfg nameref.sty nextpage.sty omllmm.fd omslmsy.fd omxlmex.fd ot1lmr.fd parskip.sty pd1enc.def pdfescape.sty pdfprivacy.sty pdftex.def pdftexcmds.sty puenc.def refcount.sty rerunfilecheck.sty size10.clo snapshot.sty supp-pdf.mkii t1lmr.fd t1lmtt.fd textcomp.sty trig.sty ts1lmr.fd umsa.fd umsb.fd uniquecounter.sty upquote.sty url.sty xcolor.sty xurl.sty'


# Checking for the '--listdeps' command line argument here before the script reaches
# a possible stopping point because it should be able to exit cleanly with
# none of the above requirements present.
if
    [ "${1}" = '--listdeps' ]

then
    echo '[info] Required programs other than shell core functionality:'
    echo "${PROGRAM_DEPENDENCIES}"
    echo '[info] Required LaTeX files:'
    echo "${LATEX_DEPENDENCIES}"
    echo "[info] Finished --listdeps run of ${0}."

    exit 0

fi


# Testing if source files required to build the PDF exist.
echo '[info] Looking for source files:'
for FILE_NAME
in \
    "${FILE_NAME_PAPER}" \
    "${FILE_NAME_HEADERINCLUDES}"
do

    if
        [ -e "${FILE_NAME}" ]

    then
        echo "[ ok ] ${FILE_NAME} found."

    else
        echo "[FAIL] ${FILE_NAME} not found." \
             'The source files are required to build the PDF!'

        exit 1

    fi

done

# Assume all dependencies are available, variable will be checked
# later to check if we need to exit because that assumption was wrong.
DEPENDENCIES_MISSING=0

# Check if all programs are available.
echo '[info] Testing program dependencies:'

for PROGRAM_DEPENDENCY
in ${PROGRAM_DEPENDENCIES}
do
    if
        # Testing negated case to get the required positive return code
        # for the missing dependency to evaluate as true here.
        ! command -v "${PROGRAM_DEPENDENCY}" 1>/dev/null 2>/dev/null

    then
        echo "[FAIL] ${PROGRAM_DEPENDENCY} not found!"
        
        DEPENDENCIES_MISSING=1
        # Now let the loop go on to catch all cases.

    fi

done

# Using DEPENDENCIES_MISSING for status report on program dependencies.
# For explanation see the part when it's finally checked with LaTeX checks included later.
if
    [ "${DEPENDENCIES_MISSING}" -eq 0 ]

then
    echo '[ ok ] All program dependencies found.'

else
    # The missing program dependency was already listed in the loop,
    # no need to explicitly mention it again now.

    echo "${LISTDEPS_INFO}"

    exit 1

fi

# Check if all required LaTeX files are available.
echo '[info] Testing LaTeX files:'

# Shellcheck doesn't like that line but I'm not sure if the criticism applies here,
# if it does I can't see how to fix it. It does pass testing though.
if
    kpsewhich ${LATEX_DEPENDENCIES} 1>/dev/null 2>/dev/null

then
    echo '[ ok ] All LaTeX files found.'

else
    # Some file is not available, need to find out which!

    # No need for overly redundant output,
    # staying consistent with program dependency code.

    # echo '[FAIL] Some LaTeX file detected as missing! Testing individually...'

    for LATEX_DEPENDENCY
    in ${LATEX_DEPENDENCIES}
    do

        if
            # Testing negated case to get the required positive return code
            # for the missing dependency to evaluate as true here.
            ! kpsewhich "${LATEX_DEPENDENCY}" 1>/dev/null 2>/dev/null

        then
            echo "[FAIL] LaTeX file ${LATEX_DEPENDENCY} not found."
            
            DEPENDENCIES_MISSING=1
            # Now let the loop go on to catch all cases.

        fi

    done

fi

# If any dependency is missing, the variable DEPENDENCIES_MISSING will have
# been changed at least once. This will have triggered the loop for a verbose
# list. Without everything in place, inform the user about the situation
# and quit.
if
    [ "${DEPENDENCIES_MISSING}" -ne 0 ]

then
    # Tell the user how to list all dependencies.
    echo "${LISTDEPS_INFO}"

    exit 1

else
    # Everything seems to be in place, we can continue working.
    echo '[ ok ] All dependencies available.'

fi


# Test for free drive space
echo '[info] Checking available disk space:'

# df: Get the free drive space, in units of 1024 (KIBIBYTE),
# POSIX format, of the current directory.
#
# tail: Filter the output to only keep the last line with the actual numbers.
# awk: Read in the numbers as fields, print the fourth one.
if
    ! FREE_DRIVE_SPACE=$(
          df -k -P . \
        | tail -n -1 \
        | awk '{print $4}'
    )

then
    echo '[FAIL] Could not detect available disk space!'

    exit 1

else
    DRIVE_SPACE_NEEDED_IN_KIBIBYTES=$((DRIVE_SPACE_NEEDED * 1024))
    if
        [ "${FREE_DRIVE_SPACE}" -gt "${DRIVE_SPACE_NEEDED_IN_KIBIBYTES}" ]

    then
        echo "[ ok ] ${FREE_DRIVE_SPACE}K out of" \
             "${DRIVE_SPACE_NEEDED_IN_KIBIBYTES}K available."

    else
        echo "[FAIL] Only ${FREE_DRIVE_SPACE}K available!"\
             "${DRIVE_SPACE_NEEDED_IN_KIBIBYTES}K needed!"

        exit 1

    fi

fi


###
# COMMAND LINE ARGUMENTS
###


# Assume not working in any code paths until required.
MODE=''

# Handle command line arguments.
case "${1}" in

    '--debug')
        echo '[info] Working in debug mode.'
        MODE='DEBUG'

        # Fixed filenames for debug mode output.
        # Debugging means stuff might be tested repeatedly, meaning
        # iterations of overriding old files with newer files is
        # likely the expected and practical way to go.
        PROCESSED_FILE_HEADERINCLUDES="${FILE_NAME_HEADERINCLUDES}.tmp"
        PROCESSED_FILE_PAPER="${FILE_NAME_PAPER}.tmp"
     ;;

    *)
        # Explicitly inform of active code path in case of typoed argument in $1.
        echo '[info] Working in normal mode.'
        MODE='NORMAL'

        # Create temporary files for normal mode processing.
        echo '[info] Creating temporary files:'

        if
            TEMP_FILE_PAPER=$(
                mktemp "${FILE_NAME_PAPER}.tmp.XXXXXXXXXXXX"
            )

        then
            # 1 [ ok ]
            # 2 [????]
            # First temporary file was created.
            echo "[ ok ] ${TEMP_FILE_PAPER} created."

            # Try writing second temporary file.
            if
                TEMP_FILE_HEADERINCLUDES=$(
                    mktemp "${FILE_NAME_HEADERINCLUDES}.tmp.XXXXXXXXXXXX"
                )

            then
                # 1 [ ok ]
                # 2 [ ok ]
                # Both temporary files were created.
                echo "[ ok ] ${TEMP_FILE_HEADERINCLUDES} created."

                # Transfer into code path used by both normal and debug mode.
                PROCESSED_FILE_HEADERINCLUDES="${TEMP_FILE_HEADERINCLUDES}"
                PROCESSED_FILE_PAPER="${TEMP_FILE_PAPER}"

            else
                # 1 [ ok ]
                # 2 [FAIL]
                # First temporary file was created but creating second one failed!
                echo '[FAIL] Could not create temporary file for' \
                     "${FILE_NAME_HEADERINCLUDES}!"

                # Trying to exit cleanly by removing the first file again.
                if
                    rm "${TEMP_FILE_PAPER}" 1>/dev/null 2>/dev/null

                then
                    echo "[ ok ] Creation of ${TEMP_FILE_PAPER} undone."

                else
                    # Creating second file failed and first one can't be deleted,
                    # best we can do is inform the user of it now before exiting.
                    echo "[FAIL] Could not delete ${TEMP_FILE_PAPER}!"

                fi

                exit 1

            fi

        else
            # 1 [FAIL] 
            # 2 [----]
            # First temporary file couldn't be created, just exit.
            echo "[FAIL] Could not create temporary file for ${FILE_NAME_PAPER}!"

            exit 1

        fi

        # Declare a cleanup function to dispose of temporary files.
        cleanup()
        {
            # If DO_EXIT is 1, end by exiting.
            # If DO_EXIT is 0, return exit code to be handled by caller.
            # Remember to provide a value when calling!
            DO_EXIT="${1}"

            echo '[info] Cleaning up temporary files:'

            HEADERINCLUDES_FILE_REMAINS=1
            PAPER_FILE_REMAINS=1

            # Trying to delete headerincludes file.
            if
                rm "${TEMP_FILE_HEADERINCLUDES}" 1>/dev/null 2>/dev/null

            then
                echo "[ ok ] Deleted ${TEMP_FILE_HEADERINCLUDES}."
                HEADERINCLUDES_FILE_REMAINS=0

            else
                echo "[FAIL] Could not delete ${TEMP_FILE_HEADERINCLUDES}!"

            fi

            # Trying to delete paper file.
            if
                rm "${TEMP_FILE_PAPER}" 1>/dev/null 2>/dev/null

            then
                echo "[ ok ] Deleted ${TEMP_FILE_PAPER}."
                PAPER_FILE_REMAINS=0

            else
                echo "[FAIL] Could not delete ${TEMP_FILE_PAPER}!"

            fi

            # Should evaluate to 0 for success, 1 or 2 for failure.
            RETURN_VALUE=$((HEADERINCLUDES_FILE_REMAINS + PAPER_FILE_REMAINS))

            # Finish as requested by applying DO_EXIT
            # as declared at the start of the function.
            if
                [ "${DO_EXIT}" -eq 0 ]

            then
                return "${RETURN_VALUE}"

            else
                exit "${RETURN_VALUE}"

            fi
        }

        # Catch unclean shell exits to run cleanup whenever not in debug mode.
        # Can't catch 9 so don't even try.
        trap 'cleanup 1' 1 2 3 6 14 15
    ;;

esac


###
# STRINGS
###


# Save some strings into variables to have them consistent and only generate them once.

# Versioning stage. Optional.
echo '[info] Reading version file:'

if 
    [ -e version ]

then
    # Sadly not an useless use of cat since
    # $(< version) is a POSIX undefined bashism.
    DOC_VERSION=$(cat version 2>/dev/null)
    echo '[ ok ] version file read in.'

else
    echo '[info] File not found, stating version as unknown.'
    DOC_VERSION='Unknown Version'

fi

# Document Title as extracted from current first heading.
#
# grep: Extract line with first heading string from the text.
# cut: Remove the heading syntax by filtering out the first 2 characters. 
#
# Implies a single space after "#".
echo '[info] Reading document title:'

if
    DOC_TITLE=$(
          grep '^#[^#]*$' "${FILE_NAME_PAPER}" \
        | cut -c 3-
    )

then
    echo '[ ok ] Document title read in.'

else
    echo "[FAIL] Could not read title from ${FILE_NAME_PAPER}!"

    if
        [ "${MODE}" = 'NORMAL' ]

    then
        cleanup 0

        exit 1

    fi

fi

# Time and Date in UTC. Optional.
echo '[info] Getting current time:'

if
    CURRENT_TIME=$(
        date -u '+%Y %b %d %H:%M'
    )

then
    echo '[ ok ] Time read in.'

else
    echo '[info] Could not get current time, stating as unknown.'
    CURRENT_TIME='Unknown Time'

fi

# Combine DOC_VERSION with Git hash if applicable.
echo '[info] Checking for git:'
if
    command -v git 1>/dev/null 2>/dev/null

then
    echo '[ ok ] Git found.'
    echo '[info] Parsing for git commit hash if applicable:'
    if
        # Is the currect directory a Git repository?
        # No need to check if it's not, same fatal error comes with diff-files.
        # git rev-parse --is-inside-work-tree 1>/dev/null 2>/dev/null

        # Are the files in the current directory an unchanged checkout?
        git diff-files --quiet --exit-code 1>/dev/null 2>/dev/null

    then
        # The state of the files can be identified with the commit hash.
        # Try to include commit hash in version strings.
        if
            GIT_COMMIT=$(
                git rev-parse --short HEAD
            )

        then
            echo '[ ok ] Git commit hash read in.'

        else
            echo '[info] Error trying to get git commit hash. Stating as unknown.'
            GIT_COMMIT='Unknown commit'

        fi

        VERSION_STRING_DOC="${DOC_VERSION} Commit ${GIT_COMMIT}"
        VERSION_STRING_FILENAME="${DOC_VERSION}-${GIT_COMMIT}"

    else
        # The state of the files cannot be identified by a hash.
        # Leave commit hash out.
        echo "[info] Files differ from commit. Skipping."
        VERSION_STRING_DOC="${DOC_VERSION}"
        VERSION_STRING_FILENAME="${DOC_VERSION}"

    fi

else
    echo "[info] Git not found. Skipping."
    VERSION_STRING_DOC="${DOC_VERSION}"
    VERSION_STRING_FILENAME="${DOC_VERSION}"

fi


###
# PROCESSING
###


# Replace %variable% placeholders in headers and footers
# (Version, Time, Title) with their actual texts as processed above
# as well as the configured homepage.
echo "[info] Processing ${FILE_NAME_HEADERINCLUDES}" \
     "into ${PROCESSED_FILE_HEADERINCLUDES}:"

if
      sed \
          "s/%version%/${VERSION_STRING_DOC}/g
           s/%time%/${CURRENT_TIME}/g
           s/%title%/${DOC_TITLE}/g
           s/%homepage%/${URL_HOMEPAGE}/g" \
          "${FILE_NAME_HEADERINCLUDES}" \
    > "${PROCESSED_FILE_HEADERINCLUDES}"

then
    echo "[ ok ] ${PROCESSED_FILE_HEADERINCLUDES} finished."

else
    echo "[FAIL] ${FILE_NAME_HEADERINCLUDES} could not be processed!"

    if
        [ "${MODE}" = 'NORMAL' ]

    then
        cleanup 0

        exit 1

    fi

fi

# The markdown file is supposed to be easy to edit.
# Some manipulations of the LaTeX that pandoc will generate
# can be done through the yaml metadata file.
# Other manipulations seem to require LaTeX insertions in the text.
# This seems to be the most elegant way while remaining POSIX compatible.
echo "[info] Processing ${FILE_NAME_PAPER} into ${PROCESSED_FILE_PAPER}:"

# Prepare sed REGEX and replacement strings
#
# The tildes ('~') will later act as newline-substitutes
# because POSIX sed doesn't work on multiple lines at once.

# Insert \tableofcontents between introductory content and Credit,
# end it in a pagebreak for a clean start into the content.
SED_TABLEOFCONTENTS_REGEX="## ${HEADING_CREDIT}"
SED_TABLEOFCONTENTS_REPLACEMENT='\\tableofcontents~~\\clearpage~~&'

# Uncomment \clearpage out of HTML comments to insert page breaks.
SED_PAGEBREAK_REGEX='<!-- \\clearpage -->'
SED_PAGEBREAK_REPLACEMENT='\\clearpage'

# Add \hfill after all fourth and higher level headings
# to make paragraphs start on a new line.
SED_HFILL_REGEX='~####[^~]\{2,\}~'
SED_HFILL_REPLACEMENT='&~\\hfill~'

# Insert \rowcolors{1}{}{lightgray} before tables for alternating row colors.
SED_TABLE_COLOR_REGEX='~~|'
SED_TABLE_COLOR_REPLACEMENT='~~\\rowcolors{1}{lightgray}{}&'

# Replace <figure> construct (which pandoc seems to reduce to a <p>) with \epigraph.
SED_QUOTE_HTML_REGEX='<figure>[^<]*<blockquote>[^<]*<p>\([^~]*\)<\/p>[^<]*<\/blockquote>[^<]*<figcaption>\([^~]*\)<cite>\([^~]*\)<\/cite><\/figcaption>[^<]*<\/figure>'
SED_QUOTE_LATEX_REPLACEMENT='\\epigraph{\\textit{\1}}{\2\\textit{\3}}'

# Apply REGEX and replacement strings through sed
#
# sed: Turn tildes ('~') into replacement strings to free the character for meta use.
# tr: Turn newlines ('\n') into tildes ('~') so it's all one long line.
# sort: Append a newline so it's still a POSIX-valid line.
# sed: Apply changes.
# tr: Turn tildes ('~') into newlines ('\n'), restoring those.
# sed: Restore tilde replacement strings back into tildes ('~').
#
# Leaves a trailing newline that could be removed with head if needed.
if
      sed 's/~/%tilde%/g' "${FILE_NAME_PAPER}" \
    | tr '\n' '~' \
    | sort \
    | sed \
        "s/${SED_TABLEOFCONTENTS_REGEX}/${SED_TABLEOFCONTENTS_REPLACEMENT}/1
         s/${SED_PAGEBREAK_REGEX}/${SED_PAGEBREAK_REPLACEMENT}/g
         s/${SED_HFILL_REGEX}/${SED_HFILL_REPLACEMENT}/g
         s/${SED_TABLE_COLOR_REGEX}/${SED_TABLE_COLOR_REPLACEMENT}/g
         s/${SED_QUOTE_HTML_REGEX}/${SED_QUOTE_LATEX_REPLACEMENT}/g" \
    | tr '~' '\n' \
    | sed 's/%tilde%/~/g' \
    > "${PROCESSED_FILE_PAPER}"

then
    echo "[ ok ] ${PROCESSED_FILE_PAPER} finished."

else
    echo "[FAIL] ${FILE_NAME_PAPER} could not be processed!"

    if
        [ "${MODE}" = 'NORMAL' ]

    then
        cleanup 0

        exit 1

    fi

fi


###
# OUTPUT
###


# Assemble final PDF file name and implicitly tell pandoc that the output format is PDF.
OUTPUT_FILE_NAME="${OUTPUT_NAME}-${VERSION_STRING_FILENAME}.pdf"

# Generate PDF.
echo "[info] Generating PDF:"

# Assume the process will succeed, variable will be used
# later to check if we need to exit with any code but 0
# because that assumption was wrong.
GENERATION_PROCESS_EXIT_CODE=0

case "${MODE}" in

    'DEBUG')
        # Giving everything needed to run diagnosis on errors in debug mode.
        if
              pandoc \
                --verbose \
                --from=markdown \
                --metadata-file="${PROCESSED_FILE_HEADERINCLUDES}" \
                --output="${OUTPUT_FILE_NAME}" \
                "${PROCESSED_FILE_PAPER}" \
            2>"${DEBUG_LOG_FILE_NAME}"

        then
            echo '[ ok ] PDF successfully generated.'

        else
            echo '[FAIL] Could not generate PDF!'

            GENERATION_PROCESS_EXIT_CODE=1

        fi

        echo '[info] Debug mode: Possibly remaining files:'
        echo "${PROCESSED_FILE_HEADERINCLUDES}"
        echo "${PROCESSED_FILE_PAPER}"
        echo "${DEBUG_LOG_FILE_NAME}"
    ;;

    'NORMAL')
        if
            pandoc \
                --from=markdown \
                --metadata-file="${PROCESSED_FILE_HEADERINCLUDES}" \
                --output="${OUTPUT_FILE_NAME}" \
                "${PROCESSED_FILE_PAPER}"

        then
            echo '[ ok ] PDF successfully generated.'

            cleanup 0

        else
            echo '[FAIL] Could not generate PDF!'
            echo '[info] Run with --debug to leave behind files for troubleshooting.'

            cleanup 0

            exit 1

        fi
    ;;

    *)
        echo '[FAIL] Unexpected code path.'

        cleanup 0

        exit 1
    ;;

esac

# Generate SHA256 Checksum File if possible.

echo '[info] Generating checksum:'
if
    command -v sha256sum 1>/dev/null 2>/dev/null

then
    CHECKSUM_FILE="${OUTPUT_FILE_NAME}.sha256"
    if
        sha256sum "${OUTPUT_FILE_NAME}" 1>"${CHECKSUM_FILE}" 2>/dev/null

    then
        echo "[ ok ] ${CHECKSUM_FILE} generated."

    else
        echo '[info] Error trying to generate checksum. Skipping.'

    fi

else
    echo '[info] sha256sum not found. Skipping.'

fi


# Clearly mark end of execution, explicitly naming the script file as given in $0.
echo "[info] Finished ${0}."

# Debug mode exit code control
exit "${GENERATION_PROCESS_EXIT_CODE}"
