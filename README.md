# 5BL Paper README

## What this is not

* No therapy suggestions.
* No information about the current global health situation.
* Nothing is promoted or endorsed.

If you were looking for or planning to contribute any of the above, you can leave now.

## What this is

An attempt at writing a text containing exactly what the Abstract and Scope sections promise. It currently is something weird a random person on the Internet wrote. It's supposed to be a conversation starter with the ambition of turning into hard facts that are easy to digest.

This is completely free and you're invited to collaborate on improving and making it more accessible. The lack of a license indicates I don't know how to phrase this in internationally applying legalese without allowing malicious shenanigans.

## Why

I don't know if the theory is correct. But I think it's wrong to call it false without having understood it enough to know what exactly should be tested and when to safely interpret the results as having debunked or confirmed any part of it.

Depending on how you research the topic, you find mostly misinformed critics, unscientific promotions, nothing short explaining neutrally without the need for prior knowledge an experts understanding of the topic. After hours of research, writing and editing, this is the best explanation I could come up with.

## Contribute

### Experts

* Did I get anything wrong? I'm not an expert, just presenting my understanding.
* Did I miss anything important, leave out anything that makes the theory fall apart? It's not meant to be in detail but it should make sense.

### Newcomers

* Is there anything you absolutely don't understand?
* Would anything instantly make a lot more sense if I slightly rephrased it?

### Translators

While I can't and don't want to stop you from translating the text, I'd prefer to save the effort for a more final version. I'll gladly start with the German version then.

If you write a translation as its own project, it'd be cool if you sent me the URL so the translations can hyperlink to each other and people can find the one in their language easier.

## File List

_Short explanation of some important files and directories in this repository and their use:_

|File Name|Contents|
|-|-|
|README.md|Explains purpose and contents of this repository.|
|5bl_paper_en.md|The raw text easily to edit in Markdown. _(To read it comfortably, view it rendered or see a release instead.)_|
|headerincludes_pdf.yaml|Header-file used to include LaTeX code in pandoc PDF generation.|
|assets/|Directory of media files for embedding.|
|version|Versioning stage.|
|build_pdf_en.sh|Shellscript to generate a well readable PDF file from the raw text data. Should be POSIX compliant.|
|.gitlab-ci.yml|GitLab-specific file to coordinate automated PDF generation.|

## Editing

* The unrendered raw text includes HTML-comments of which some work as code, others point out where improvements could be made. Generally, read the comments if you plan on contributing.
* No therapy requests allowed. Only completed, comprehensible cases are listed in the Testimonial Archive linked in the "Example cases" section.
* In order to keep printed and badly converted versions close to the usability of a proper text PDF, keep all hyperlinks as readable text.
* Avoid (overusing) examples. The text itself should explain the idea and be understandable without turning into a list of case studies. Only use them illustratively where they add value, the text is supposed to be short.

## Usage of build_pdf_en.sh

The Shellscript is used in the release process, readers don't need it if there's a release available.

A successful run will return an Exit Code of `0`. Any other Exit Code indicates an error. For more details, read the Standard Output. For even more in depth troubleshooting, some suppressed Output can be restored by no longer redirecting it to `/dev/null`.

|Command|Description|
|-|-|
|`$ ./build_pdf_en.sh --listdeps`|Output a list of all dependencies required for a normal mode run to succeed.|
|`$ ./build_pdf_en.sh`|Execute a normal mode run to generate a PDF file from the raw text. Tries to additionally generate a separate file with a checksum for the PDF file.|
|`$ ./build_pdf_en.sh --debug`|Execute a debug mode run to try and generate a PDF file from the raw text, leaving behind files a normal mode run would generate as well as the temporary working files used and a verbose logfile.|
